pragma solidity ^0.5.1;

contract Voting {

    struct Voter {
        bool ifVote;
        uint256 voterId;
        uint8 v;
        bytes32 r;
        bytes32 s;
        bytes32 voteHash;
        bytes32 optionHash;
        bytes32 secreateHash;
    }

    address public admin;
    address public thirdParty;
    uint256 public votersCount = 0;

    mapping(address => Voter) public voters;
    
    mapping(uint256 => address) public listStore;

    constructor(address _thirdParty) public {
        admin = msg.sender;
        thirdParty = _thirdParty;
    }
    function addVoter(address _voter) public pure {
        require(msg.sender == admin || msg.sender == thirdParty);

        voters[_voter].voterId = votersCount;
        listStore[votersCount] = _voter;
        votersCount++;
    }

    function voterVote(bytes32 voteHash, uint8 v, bytes32 r, bytes32 s) public pure {
        require(!voters[msg.sender].ifVote);

        require(msg.sender == ecrecover(voteHash, v, r, s));

        voters[msg.sender].ifVote = true;
        voters[msg.sender].v = v;
        voters[msg.sender].r = r;
        voters[msg.sender].s = s;
        voters[msg.sender].voteHash = voteHash;
    }

    

}
