const NodeRSA = require('node-rsa');

// console.log(key);
// let testing = key.exportKey('pkcs8-private-pem');
// console.log(testing);
// const text = 'Hello RSA!';
// const encrypted = key.encrypt(text, 'base64');
// console.log('encrypted: ', encrypted);
// const decrypted = key.decrypt(encrypted, 'utf8');
// console.log('decrypted: ', decrypted);

function newKey() {
  let key = new NodeRSA({ b: 2048 });
  return key;
}

function newKeyAndExport() {
  let key = new NodeRSA({ b: 2048 });
  return key.exportKey('pkcs8-private-der');
}

// let output = Buffer.from(newKeyAndExport()).toString('hex');
// console.log(output);

function newKeyformHex() {
  let query = `308204be020100300d06092a864886f70d0101010500048204a8308204a402010002820101008b0816a60553821c78af92d8272df8f21083917a627f8eb7edab57d2b66450f895eff825b6f447068c033736fe92193e6da8a0064659908f3d07f94f88262e991f40f1711a2cdbe159c5648d4553bbc47bb859ac9e02e2f6a7269c95d0e0df7558153fec782476cd92f5d41b1499bcc2d09dcfa0d06ec5a51bb3a4fecc2c1215e6bebf72d1edbe7b4351db9f1fdc25ca117807d2c0f8ff04e2671960452f6fa9b12d64c74819decf887e5f27c479ebb7452784a408d9830d37276d1dfbeb22ed24ed58c7a4bbe2d8313134703820e26dc6043f81cc9bc2f160b7472e1a6328e1e174e15dad3aa2064312e0f58e3a8179660918a33971d0239ab0d8947910891302030100010282010070a87839c55d1ab45972e3c9b54dc248dbead0ca5be042d47588ff1c2661795f4597f2fb149a32a77272f49de2809e6600d3bffff5a783c51dce454e15e3e2a47f37cfad37699c2ea9e0928f0adb43ee7dd62f0bf9ea1ae185267b81c685416d4397cc79a00ce41bc676de8da70ec06f183e984d16387c7d42ca39eba1a4206e5d98014882bb64f54edf03e954815f11c17a4514e667655a46135a52d4b1931b89ede727a2fa5a1853161ffe48f6badc37dff646b7611c49c94dd35e9f1153f217e53121afbf62fcccf12099554f814e48f9f1ea6f21f07dfdd15ac27b89d1cf66b724644d5e2c634547e25a96be74d5131f0986108e474e8ce18cc220d19d9102818100d01e8e9bdd30aa37a1e2a0f1dba392ad3a292dfec4bf3bd13a0dcea4c60125b141f54c3db313f330ed0b3364050f7212df21a1b1ffaf0511c971a0818501a11598f65384636ea86c178eeb44f063c8eb6782a5a1f422ff411a0de429620dad7d5ab87fa02e172d887f4ac617d802c3c3fc93ac49469cb5ee0d3ae8de82ddaa6902818100ab0485f83b98fd105ab31fc6a79fbe41bb12caed394988306a842a2d813b82128eabf1ed24625760791ff1374c72cd1f15c5e0996f2345bc45409dae1c5a1424397df8292a975ef3c0d4170d2e3bc386483bda0df6e35eac24ab70067a6812d2842b2dc7ff44ce2e92ab126eb75386411851316b272d9736c7eb8adadda1101b02818100b384481aa5d69e5cd264bbd323539ae7952d842127f094564b2f31b9eb997dbbc25b26626a1fa13be89c2abdf1a77b0c502fa05bfb622a8b47299dfb5a9127338bf79ddeaa2dff550d9517d7254bcd3950d5dead2dfb2a162bb523c3967299dd70ed8818d0e8e93bbe20ef720dbf84c17f129a1fb2aae63aea6a9007aef62951028181009ccfc7fc06c1d081f778b9d1511073c717bf329d74e30ce2c038b1e809d0c4033a1945fb195aa71e09d2953f0c7553ca860f6c53a489a63eed0d43c84bacf93d020e94c7428b8c005c36dd70338d8f4a88df80a13f59db0c638f63683d4342c70f73fc459fb6ba8c0fe6973f3810795124f8062567c6311a851460bd316da7330281802c8af4c62383bd84c3d86648566a914592873dc4f5232106350617fca636b10b39b1394efeb773d862d543b1c183b48f21a890d270c1bf728f4fbb8c61f0bbacff5e1d71a2b15986d9307860b1c79757fcc9e50b3a758af121cae107462ff4a4bcae2d59cdc91f713981e58a540296b68bed9b069f996c737931961c11f0ac22`;
  let sample = Buffer.from(query, 'hex');
  let key = new NodeRSA(sample, 'pkcs8-private-der');

  let text = 'hello rsa';
  let text1 = 'try';
  let encrypted = key.encrypt(text);
  console.log('encrypted: ', encrypted);
  let signed = key.sign('text try');
  console.log('signed: ', signed);
  let verified = key.verify('text try', signed);
  return verified;
  // let decrypted = key.decrypt(encrypted, 'utf8');
  // console.log('decrypted: ', decrypted);
  // // return key.isPrivate();
}

console.log(newKeyformHex());

module.exports = { NodeRSA, newKey, newKeyAndExport };
