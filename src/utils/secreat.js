const secrets = require('secrets.js-grempe');

var key = secrets.random(512);
console.log(key);

var shares = secrets.share(key, 10, 5);

console.log(shares.slice(0, 4));

var comb = secrets.combine([
  shares[2],
  shares[0],
  shares[5],
  shares[1],
  shares[7],
]);
console.log(comb === key);
