let keythereum = require('keythereum');

var params = { keyBytes: 32, ivBytes: 16 };
const ethereumjsWallet = require('ethereumjs-wallet');

// synchronous
// var dk = keythereum.create(params);
// let result = keythereum.create(params);

// let password = 'qwer';
// let options = {
//   kdf: 'pbkdf2',
//   cipher: 'aes-128-ctr',
//   kdfparams: {
//     c: 262144,
//     dklen: 32,
//     prf: 'hmac-sha256',
//   },
// };
// var keyObject = keythereum.dump(
//   password,
//   dk.privateKey,
//   dk.salt,
//   dk.iv,
//   options,
// );

// console.log(result.privateKey.toString('hex'));
let passphraseInput = 'jack850912';
let options = {
  kdf: 'pbkdf2',
};

const generatedWallet = ethereumjsWallet.generate();
const keyJSON = generatedWallet.toV3(passphraseInput, options);
// let resultPrivateKey = generatedWallet.getPrivateKey()
const address = generatedWallet.getAddress().toString('hex');
const filename = 'UTC--' + new Date().toISOString() + '--' + address;

console.log(generatedWallet, keyJSON, address, filename);
