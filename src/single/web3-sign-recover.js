const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

function createAddress() {
  // let newAddress = web3.eth.accounts.create();
  // console.log(newAddress);
}

async function main() {
  let result = await web3.eth.accounts.sign(
    'Some data',
    '0x8d5e58aaeb9fc507068a793cc538d224ebce115e3de4e1417d5334ccccd0738d',
  );
  console.log(result);

  let recover = await web3.eth.accounts.recover('Hello', result.signature);

  console.log(recover);
}
main();
