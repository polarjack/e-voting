const NodeRSA = require('node-rsa');
const key = new NodeRSA({ b: 512 });

let publicKey = `
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr3IoNL6r9K3Fb6hhShi8
Fl6fy1ycThHb1PQWhN9etLfIfuIAi1bbOplc1i6HKCT/4fH3Cil/AlHOt8s0xJip
S3ywYKe3AWkrobVEz0gV/CqQY8aGtV7NmjdgrWAVekPaQe+A+wlc4nKgTvCS/EJE
CEocOnZpsGciPOlmg2de7FWwE5chngqgDPv2nUZ+Hc/Tp4noM+QsN1J2Zl2dhmsG
CfOPxlJ1RVd57d+8Grk0N8+cuyNCvcc5+1Hlu1pzk7NhhUSEVhWBDMLfCSo2ri5X
D+3FVG6Fmd36agmZSPli2IumunxHZ9qRhGi33koBiYOmiOLXRsjxQy9o9JoS8rlU
nwIDAQAB
-----END PUBLIC KEY-----
`;

let privateKey = `
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAr3IoNL6r9K3Fb6hhShi8Fl6fy1ycThHb1PQWhN9etLfIfuIA
i1bbOplc1i6HKCT/4fH3Cil/AlHOt8s0xJipS3ywYKe3AWkrobVEz0gV/CqQY8aG
tV7NmjdgrWAVekPaQe+A+wlc4nKgTvCS/EJECEocOnZpsGciPOlmg2de7FWwE5ch
ngqgDPv2nUZ+Hc/Tp4noM+QsN1J2Zl2dhmsGCfOPxlJ1RVd57d+8Grk0N8+cuyNC
vcc5+1Hlu1pzk7NhhUSEVhWBDMLfCSo2ri5XD+3FVG6Fmd36agmZSPli2IumunxH
Z9qRhGi33koBiYOmiOLXRsjxQy9o9JoS8rlUnwIDAQABAoIBAB2YRR34l+sTMP3P
ynGEUJ35Yj0cpyUoMAvxDKdnQoN7iO+WUf8vLJXq7tUWTihAbYWAmmp1a35JrHQm
bAVJty0LoHj9cYpkYj7J5AyPHV5HE8ow3+JSVp5P/xff4F+aeIOs0fYGs0tjiJUo
UbMJvo4jajWWaeeezhC2UluSIVn+JoLUeVVDuGofl3uf+vIZxEXA8S1zjWLOxA5B
1xTouZQyn2j1XoJnxFMeLT2760bahx0jvVyQU4xfaIcVIV8uhH1Y5cSTCIfqvjG6
S8tbdY9xq9WP/wINMCVXjNL5IhGK+HPU1UshgZsFqznRBmxsN4FiTs3EUIMKkScN
JRKHZTECgYEA6Ga4xrKYsSlka7wOco1rSKDEJdGUhDS5plHsGM+szVu4HX0D19kl
046Ep2GRVzLLwpHSq5e+qNAq+A7snGYVUoCjtT2G6otcuwaR+T7yP1IzohTcbEmU
+7kaYrPXQN2ZbCW6eS5we1l6MKEjEPwCY1clj/PisZ0PxaSJ8ByLkxcCgYEAwULh
LTDYXR6kW37iQjZpWeAYbmJ6wU/19ATWi1ewMz2Rqzc/8hzgAThHRgDYjPK5wC/f
IglxHjl2zIw5xfE0GaCI7nZwmORLwygqw9huTISd4TjzpVew2WVtOr0aa6ZmONlR
cffE5B0CESZ/sdVYmb20ShPo57ynSW0kKfn937kCgYBtTiMKEWYPrVA1+7Uv26Ph
/PtEblZT+Bh5SX6qDMNRsxndhLcSBmfsINPzhcg+Iv4C/WMAodZiv8X19c8DoF/W
9foJfG7AUdvDe0uc1yZI8fim+vjRVapk2qK2Wa+cKp3GZ9hUtqrg2SzHQoKjsmPN
JWfdVPACacTJSKI39LUQ8QKBgBgRnj2kka23PTzv5nnckO5SjHhi52rf7H/Bgn7E
IvuetV+fd+VNV1CPk8DSJiYXrK6Ux7NTfbUH7xW2eRN/4L84URVxxsAj3pOlkv3l
HIeHFKqSDzYOX8YTbbEdEAdMJ/r4hb4t32WRf0pngwvAyFINYEDBjgYeZq8sWo6n
xchxAoGBANgFfxjjuTjgWC4Wj2XfXLV6W1Gqdp/xx4mjMD4CDbL7CgmP0Jcysbj1
hoqTrjAVrsE4qaU67iSGnitZzF87IuuGYVse+JlmdtJO/HLEHj1f+54w32Alepg/
6I3wSfgtTc+GUNoKVCnTzdIm23qWc7jJS68Z4Dq4OsRMNT56EQ/J
-----END RSA PRIVATE KEY-----
`;

let tmp = key.importKey(publicKey, 'public');

console.log(tmp);
let encrypt = tmp.encrypt('option', 'base64');

console.log(encrypt);
let otherKey = key.importKey(privateKey);
let decrypt = otherKey.decrypt(encrypt, 'utf8');

console.log(decrypt);
