const fs = require('fs');

const NodeRSA = require('node-rsa');
const key = new NodeRSA({ b: 512 });

function main() {
  let toWriteObject = {
    companys: [],
  };
  for (let i = 0; i < 10; i++) {
    let pairResult = key.generateKeyPair();
    let privatePair = pairResult.exportKey();
    let publicPair = pairResult.exportKey('public');
    toWriteObject.companys.push({
      rsaPub: publicPair,
      rsaPk: privatePair,
    });
  }

  fs.writeFileSync('basicUser.json', JSON.stringify(toWriteObject));
}

main();

// const text = 'Hello RSA!';
// const encrypted = key.encrypt(text, 'base64');
// console.log('encrypted: ', encrypted);
// const decrypted = key.decrypt(encrypted, 'utf8');
// console.log('decrypted: ', decrypted);
