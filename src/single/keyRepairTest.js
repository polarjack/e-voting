const NodeRSA = require('node-rsa');
const key = new NodeRSA({ b: 512 });
const { companys } = require('./basicUser.json');

function singleTry(publicKey, privateKey) {
  let tmp = key.importKey(publicKey, 'public');

  let encrypt = tmp.encrypt('option', 'base64');

  let otherKey = key.importKey(privateKey);
  let decrypt = otherKey.decrypt(encrypt, 'utf8');

  return decrypt;
}

function main() {
  companys.map((v, i) => {
    console.log(v, i);
    const result = singleTry(v.rsaPub, v.rsaPk);
    console.log(result);
  });
}

main();
