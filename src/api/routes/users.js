'use strict';

const Router = require('express-promise-router');
const router = new Router();

const { client } = require('../../../config/postgresql');

const { NodeRSA, newKey, newKeyAndExport } = require('../../utils/NodeRsaTry');

const secrets = require('secrets.js-grempe');

router.get('/', async (req, res) => {
  let { rows } = await client.query('select NOW()');
  res.json({
    status: 'success',
    data: rows[0],
  });
});

router.get('/init', async (req, res) => {
  let trail = await newKeyAndExport();
  let query = `insert into users (user_rsa) values ($1)`;
  let { rows } = await client.query(query, [trail]);
  res.json({
    status: 'success',
    data: trail,
    db: rows[0],
  });
});

router.get('/getAllUser', async (req, res) => {
  let query = `select * from users where id = 'be83340c-df09-4be0-9115-b24e87bfa9d3'`;
  let { rows } = await client.query(query);

  rows.map((v, i) => {
    console.log('number: ', i);
    let key = v.user_rsa;
    console.log(key);
    // let key = new NodeRSA(v.user_rsa);
    // let text = 'Hello RSA!';
    // let encrypted = after.encrypt(text, 'base64');
    // console.log('encrypted: ', encrypted);
    // let decrypted = after.decrypt(encrypted, 'utf8');
    // console.log('decrypted: ', decrypted);

    // var key = secrets.random(512);
    // console.log(key);

    var shares = secrets.share(key, 10, 5);

    console.log(shares.slice(0, 4));

    var comb = secrets.combine([
      shares[2],
      shares[0],
      shares[5],
      shares[1],
      shares[7],
    ]);
    console.log(comb === key);
  });
  res.json({
    data: rows,
  });
});

router.get('/allUserList', async (req, res) => {
  let query = 'select * from users';
  let { rows } = await client.query(query);
  res.json({
    data: rows,
    status: 'success',
  });
});

module.exports = router;
