'use strict';

const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.send('this is e-voting system');
});

router.get('/init', (req, res) => {
  res.send('this is voting init');
});

module.exports = router;
